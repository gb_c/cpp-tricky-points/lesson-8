#ifndef PHONENUMBER_H
#define PHONENUMBER_H

#include <iostream>
#include <tuple>
#include <optional>


class PhoneNumber
{
private:
    int m_countryCode;                                                          // Код страны
    int m_cityCode;                                                             // Код города
    std::string m_number;                                                       // Номер
    std::optional<int> m_extention;                                             // Добавочный номер

public:
    PhoneNumber(int countryCode,
                int cityCode,
                std::string number,
                std::optional<int> extention);                                  // Конструктор

    friend std::ostream &operator<<(std::ostream &os, const PhoneNumber &p);    // Оператор <<
    friend bool operator<(const PhoneNumber &p1, const PhoneNumber &p2);        // Оператор <
    friend bool operator==(const PhoneNumber &p1, const PhoneNumber &p2);       // Оператор ==
};

#endif // PHONENUMBER_H
