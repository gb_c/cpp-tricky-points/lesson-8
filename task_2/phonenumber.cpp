#include "phonenumber.h"


PhoneNumber::PhoneNumber(int countryCode, int cityCode, std::string number, std::optional<int> extention)
    : m_countryCode(countryCode)
    , m_cityCode(cityCode)
    , m_number(number)
{
    m_extention = (extention != 0?extention:std::nullopt);
}


// operator<< ...
std::ostream& operator<<(std::ostream &os, const PhoneNumber &p)
{
    os << '+' << p.m_countryCode << '(' << p.m_cityCode << ')' << p.m_number << ' ';

    // Если добавочный код существует
    if(p.m_extention) os << *p.m_extention;

    return os;
}


// operator< ...
bool operator<(const PhoneNumber &p1, const PhoneNumber &p2)
{
    return tie(p1.m_countryCode, p1.m_cityCode, p1.m_number, p1.m_extention) <
           tie(p2.m_countryCode, p2.m_cityCode, p2.m_number, p2.m_extention);
}


// operator== ...
bool operator==(const PhoneNumber &p1, const PhoneNumber &p2)
{
    return tie(p1.m_countryCode, p1.m_cityCode, p1.m_number, p1.m_extention) ==
           tie(p2.m_countryCode, p2.m_cityCode, p2.m_number, p2.m_extention);
}

