#ifndef PHONEBOOK_H
#define PHONEBOOK_H

#include <iostream>
#include <fstream>
#include <cassert>
#include <vector>
#include <algorithm>

#include "person.h"
#include "phonenumber.h"


class PhoneBook
{
private:
    std::vector<std::pair<Person, PhoneNumber>> book;

    static bool sortBySec(const std::pair<Person, PhoneNumber> &a,
                          const std::pair<Person, PhoneNumber> &b);             // Функция вспомогательная для сортировки пары по второму объекту

public:
    PhoneBook(std::ifstream &is);                                               // Конструктор (по-моему крайне костыльный получился,
                                                                                //    сходу не придумал как нормально его реализовать)
    void SortByPhone();                                                         // Функция сротировки по номеру
    void SortByName();                                                          // Функция сортировки по имени
    std::pair<std::string, PhoneNumber>
                    GetPhoneNumber(const std::string &surname);                 // Функция возврата номера телефона или ошибки
    void ChangePhoneNumber(Person person, PhoneNumber number);                  // Функция смены номера телефона

    friend std::ostream &operator<<(std::ostream &os, const PhoneBook &p);      // Оператор <<
};

#endif // PHONEBOOK_H
