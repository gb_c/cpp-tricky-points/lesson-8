#include "phonebook.h"


PhoneBook::PhoneBook(std::ifstream &is)
{
    // Если открыт файл
    if(is.is_open() && is.good())
    {
        std::string line, surname, name, patronymic, number;
        int countryCode, cityCode, extention;

        while(!is.eof())
        {
            getline(is, line);
            std::istringstream input{line};

            if(!line.empty())
            {
                input >> surname >> name >> patronymic >> countryCode >> cityCode >> number >> extention;

                book.push_back(std::make_pair(Person(surname, name, patronymic),
                                          PhoneNumber(countryCode, cityCode, number, extention)
                                         ));
            }
        }

        is.close();
    }
    else
    {
        std::cout << "File didn't open" << std::endl;
        assert(1);
    }
}


// sortBySec ...
bool PhoneBook::sortBySec(const std::pair<Person, PhoneNumber> &a, const std::pair<Person, PhoneNumber> &b)
{
    return (a.second < b.second);
}


// SortByPhone ...
void PhoneBook::SortByPhone()
{
    std::sort(book.begin(), book.end(), sortBySec);
}


// SortByName ...
void PhoneBook::SortByName()
{
    std::sort(book.begin(), book.end());
}


// GetPhoneNumber ...
std::pair<std::string, PhoneNumber> PhoneBook::GetPhoneNumber(const std::string &surname)
{
    PhoneNumber *pn = nullptr;
    uint8_t countNumber = 0;

    for(const auto& [person, number] : book)
    {
        if(person.getSurname() == surname)
        {
            ++countNumber;

            if(countNumber > 1)
                return {"found more than 1", PhoneNumber{0,0,"",std::nullopt}};
            else
                pn = new PhoneNumber(number);
        }
    }

    if(pn)
    {
        auto resultPn{*pn};

        delete pn;

        return {"", resultPn};
    }

    return {"not found", PhoneNumber{0,0,"", std::nullopt}};
}


// ChangePhoneNumber ...
void PhoneBook::ChangePhoneNumber(Person person, PhoneNumber number)
{
    for(auto& [per, num] : book)
    {
        if(per == person)
        {
            num = number;
        }
    }
}


// operator<< ...
std::ostream& operator<<(std::ostream &os, const PhoneBook &b)
{
    for(const auto& [person, number] : b.book)
    {
        os << person << '\t' << number << std::endl;
    }

    return os;
}
