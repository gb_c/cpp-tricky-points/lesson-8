#ifndef PERSON_H
#define PERSON_H

#include <iostream>
#include <iomanip>
#include <tuple>


class Person
{
private:
    std::string m_surname;                                                  // Фамилия
    std::string m_name;                                                     // Имя
    std::string m_patronymic;                                               // Отчество

public:
    Person(std::string surname,
           std::string name,
           std::string patronymic = "-");                                   // Конструктор
    std::string getSurname() const;                                         // Функция получения фамилии

    friend std::ostream &operator<<(std::ostream &os, const Person &p);     // Оператор <<
    friend bool operator<(const Person &p1, const Person &p2);              // Оператор <
    friend bool operator==(const Person &p1, const Person &p2);             // Оператор ==
};

#endif // PERSON_H
