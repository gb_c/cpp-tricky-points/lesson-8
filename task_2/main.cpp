#include "phonebook.h"
#include<gtest/gtest.h>


class TestPhoneBook : public testing::Test
{
protected:
    PhoneBook *book;

    std::stringstream buffer{};
    std::streambuf *sbuf;

    TestPhoneBook() : sbuf{nullptr}
    {}

    ~TestPhoneBook() override = default;

    void SetUp() override
    {
        sbuf = std::cout.rdbuf();
        std::cout.rdbuf(buffer.rdbuf());

        std::ifstream file("PhoneBook.txt");
        book = new PhoneBook(file);
    }

    void TearDown() override
    {
        std::cout.rdbuf(sbuf);
        sbuf = nullptr;

        delete book;
    }
};


TEST_F(TestPhoneBook, operator_cout)
{
    std::string expected{"        Ilin         Petr      Artemovich\t+7(17)4559767 \n"
                         "      Makeev        Marat                \t+77(4521)8880876 999\n"
                         "      Ivanov         Ivan   Vladimirovich\t+7(273)5699819 5543\n"};
    std::cout << *book;
    std::string actual{buffer.str()};
    EXPECT_EQ(expected, actual);
}


TEST_F(TestPhoneBook, sortByPhone)
{
    std::string expected{"        Ilin         Petr      Artemovich\t+7(17)4559767 \n"
                         "      Ivanov         Ivan   Vladimirovich\t+7(273)5699819 5543\n"
                         "      Makeev        Marat                \t+77(4521)8880876 999\n"};
    book->SortByPhone();
    std::cout << *book;
    std::string actual{buffer.str()};
    EXPECT_EQ(expected, actual);
}


TEST_F(TestPhoneBook, sortByName)
{
    std::string expected{"        Ilin         Petr      Artemovich\t+7(17)4559767 \n"
                         "      Ivanov         Ivan   Vladimirovich\t+7(273)5699819 5543\n"
                         "      Makeev        Marat                \t+77(4521)8880876 999\n"};
    book->SortByName();
    std::cout << *book;
    std::string actual{buffer.str()};
    EXPECT_EQ(expected, actual);
}


TEST_F(TestPhoneBook, changePhoneNumber)
{
    std::string expected{"        Ilin         Petr      Artemovich\t+7(17)4559767 \n"
                         "      Makeev        Marat                \t+16(465)9155448 13\n"
                         "      Ivanov         Ivan   Vladimirovich\t+7(273)5699819 5543\n"};

    book->ChangePhoneNumber(Person{"Kotov", "Vasilii", "Eliseevich"},
                               PhoneNumber{7, 123, "15344458", std::nullopt});
    book->ChangePhoneNumber(Person{"Makeev", "Marat"},
                               PhoneNumber{16, 465, "9155448", 13});
    std::cout << *book;
    std::string actual{buffer.str()};
    EXPECT_EQ(expected, actual);
}


TEST_F(TestPhoneBook, printPhoneNumber)
{
    auto print_phone_number = [&](const std::string& surname)
    {
        std::cout << surname << "\t";
        auto answer = book->GetPhoneNumber(surname);
        if(std::get<0>(answer).empty())
            std::cout << std::get<1>(answer);
        else
            std::cout << std::get<0>(answer);
        std::cout << std::endl;
    };

    // Вызовы лямбды
    print_phone_number("Ivanov");
    EXPECT_EQ("Ivanov\t+7(273)5699819 5543\n", std::string(buffer.str()));

    buffer.str(std::string());

    print_phone_number("Petrov");
    EXPECT_EQ("Petrov\tnot found\n", buffer.str());
}


int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();


//    std::ifstream file("PhoneBook.txt"); // путь к файлу PhoneBook.txt
//    PhoneBook book(file);
//    std::cout << book;

//    std::cout << "------SortByPhone-------" << std::endl;
//    book.SortByPhone();
//    std::cout << book;

//    std::cout << "------SortByName--------" << std::endl;
//    book.SortByName();
//    std::cout << book;

//    std::cout << "-----GetPhoneNumber-----" << std::endl;

//    // лямбда функция, которая принимает фамилию и выводит номер телефона этого человека, либо строку с ошибкой
//    auto print_phone_number = [&book](const std::string& surname)
//    {
//        std::cout << surname << "\t";
//        auto answer = book.GetPhoneNumber(surname);
//        if(std::get<0>(answer).empty())
//            std::cout << std::get<1>(answer);
//        else
//            std::cout << std::get<0>(answer);
//        std::cout << std::endl;
//    };

//    // вызовы лямбды
//    print_phone_number("Ivanov");
//    print_phone_number("Petrov");

//    std::cout << "----ChangePhoneNumber----" << std::endl;
//    book.ChangePhoneNumber(Person{"Kotov", "Vasilii", "Eliseevich"},
//                           PhoneNumber{7, 123, "15344458", std::nullopt});
//    book.ChangePhoneNumber(Person{"Mironova", "Margarita", "Vladimirovna"},
//                           PhoneNumber{16, 465, "9155448", 13});
//    std::cout << book;
}
