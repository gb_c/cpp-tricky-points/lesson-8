#include "person.h"


Person::Person(std::string surname, std::string name, std::string patronymic)
    : m_surname(surname)
    , m_name(name)
    , m_patronymic(patronymic)
{}


// getSurname ...
std::string Person::getSurname() const
{
    return m_surname;
}


// operator<< ...
std::ostream& operator<<(std::ostream &os, const Person &p)
{
    return os << std::setw(12) << p.m_surname << ' '
              << std::setw(12) << p.m_name << ' '
              << std::setw(15) << ((p.m_patronymic != "-")?(p.m_patronymic):"");
}


// operator< ...
bool operator<(const Person &p1, const Person &p2)
{
    return tie(p1.m_surname, p1.m_name, p1.m_patronymic) <
           tie(p2.m_surname, p2.m_name, p2.m_patronymic);
}


// operator== ...
bool operator==(const Person &p1, const Person &p2)
{
    return tie(p1.m_surname, p1.m_name, p1.m_patronymic) ==
           tie(p2.m_surname, p2.m_name, p2.m_patronymic);
}

